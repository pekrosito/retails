import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ShoppingcartComponent }   from './app/shoppingcart/shoppingcart.component';
import { ProductComponent } from './app/product/product.component';

const routes: Routes = [
  { path: '', redirectTo: '/Home', pathMatch: 'full' },
  { path: 'Home', component: ProductComponent },
  { path: 'shoppingCart', component: ShoppingcartComponent },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}