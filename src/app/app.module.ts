// Modulos de Angular para funcionalidades de Angular
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Ng2FilterPipeModule } from 'ng2-filter-pipe';
import { BrowserModule } from '@angular/platform-browser';
import { MessageService } from './_services/index';
import { dataService } from './data/index'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NotificationsService, SimpleNotificationsModule } from 'angular2-notifications';
import { MatTooltipModule, MatTabsModule, MatSlideToggleModule, MatSnackBarModule,
         MatIconModule, MatDialogModule, MatButtonModule, MatListModule, MatCardModule,
         MatToolbarModule, MatProgressSpinnerModule, MatProgressBarModule, MatInputModule,
         MatFormFieldModule, MAT_PLACEHOLDER_GLOBAL_OPTIONS, MatDialogRef, MatDialog
       } from '@angular/material';

// Componentes Internos
import { AppRoutingModule } from '../app.routing.module';
import { NavComponent } from './nav/nav.component';
import { AppComponent } from './home/app.component';
import { ModalsComponent } from './modals/modals.component';
import { ShoppingcartComponent } from './shoppingcart/shoppingcart.component';
import { ProductComponent } from './product/product.component';
import { popUpSizes } from './product/product.component';
import { popUpMap } from './product/product.component';

@NgModule({
  declarations: [
    NavComponent,
    AppComponent,
    ModalsComponent,
    ShoppingcartComponent,
    ProductComponent,
    popUpSizes,
    popUpMap
  ],
  entryComponents: [    
    popUpSizes,
    popUpMap
  ],
  imports: [
     BrowserModule
    ,Ng2FilterPipeModule
    ,FormsModule
    ,MatFormFieldModule
    ,MatInputModule
    ,BrowserAnimationsModule
    ,SimpleNotificationsModule.forRoot()
    ,MatDialogModule
    ,MatSnackBarModule
    ,AppRoutingModule
  ],
  providers: [
   // {provide: MAT_PLACEHOLDER_GLOBAL_OPTIONS, useValue: {float: 'always'}},
     MessageService
    ,dataService
  ],
  bootstrap: [NavComponent,AppComponent]
})
export class AppModule { }
export class PipeModule {
     static forRoot() {
        return {
            ngModule: PipeModule,
            providers: [],
        };
     }
   } 