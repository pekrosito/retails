import { shopping } from '../data/data.entity';
import { MatSnackBar } from '@angular/material';
import { Subscription } from 'rxjs/Subscription';
import { dataService } from '../data/data.service';
import { MessageService } from '../_services/index';
import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})

export class NavComponent implements OnDestroy {
  car = 0;
  message: any;
  subscription: Subscription;
  shoppingCart: shopping[];
  shop = Array<any>();

  constructor(private messageService: MessageService, public snackBar: MatSnackBar, private route: ActivatedRoute,private router: Router, private dataService: dataService) {     
    this.subscription = this.messageService.getMessage()
      .subscribe(
        data => { 
          if(data.data.id>=0){
            this.dataService.delShopping(data.data)
            var id = this.shop.findIndex(x=>x.data.girl == data.data.girl && x.data.color == data.data.color)
            this.shop.splice(id,1)
            this.car--
          }else{
            if(!this.shop.find(cart=> cart.data.girl === data.data.girl && cart.data.color === data.data.color)) {
                this.shop.push(data);
                this.dataService.setShopping(data.data);
                this.car++;
              }else{
                this.snackBar.open('Ya existe el producto', 'Cerrar', {
                duration: 2000,
                })
              }
            }
          }
        );
      }

  ngOnDestroy() {
      this.subscription.unsubscribe();
  }

  viewShoppingCart() {
    if(!this.shop.length) {
        this.snackBar.open('No posee productos agregados', 'Cerrar', {
          duration: 2000,
        })
      }else{
        this.router.navigate(['/shoppingCart']);
      }
  }
}