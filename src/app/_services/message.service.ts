import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class MessageService {
    private subject = new Subject<any>();

    sendMessage(data: object) {
        this.subject.next({ data });
    }

    clearMessage(data: object) {
        this.subject.next({ data });
    }

    getMessage(): Observable<any> {
        return this.subject.asObservable();
    }
}