import { Component, OnInit } from '@angular/core';
import { dataService } from '../data/data.service';
import { MessageService } from '../_services/index';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { shopping, product, picForProduct } from '../data/data.entity';

@Component({
  selector: 'app-shopping',
  templateUrl: './shoppingcart.component.html',
  styleUrls: ['./shoppingcart.component.css']
})
export class ShoppingcartComponent implements OnInit {
  shoppings: shopping[];
  //picForProduct: picForProduct[];
  product: product[];
  cart= Array<any>();
  totPrice=0;
  quantity=0;

  constructor(private dataService: dataService, private router: Router, private messageService: MessageService) { }

  cutGirl(id){
    var indx = this.cart.findIndex(x=>x.id == id)
    var data = this.cart[indx]
    this.messageService.clearMessage(data);
    this.cart.splice(indx,1)
    this.totPrice = 0
    this.cart.forEach(parameter=>{
      this.totPrice += parameter.price
    })
    this.quantity--
    
    if(this.quantity==0)
      this.router.navigate(['/Home']);
  }

  goBack(){
    this.router.navigate(['/Home']);
  }

  ngOnInit() {
    this.getComponents()
    setTimeout(() =>(this.setValues()), 800);
  }

  getComponents(){
    this.dataService.getShopping().then(shopping => this.shoppings = shopping)
    this.dataService.getProducts().then(product => this.product = product);
  }

  setValues(){
    if(!this.shoppings.length){
      this.router.navigate(['/Home']);
    }
    var id = 0
    
    this.shoppings.forEach(parameter => {
      var dataGirl = {picture:'',color:'',size:'',details:'',price:0,id:0, girl:0}
      var object = this.product.find(obj => {return obj.idGirl == parameter.girl})
      dataGirl.color = parameter.color
      dataGirl.picture = parameter.picture
      dataGirl.size = parameter.size
      dataGirl.details = object.detailProduct
      dataGirl.price = object.price
      dataGirl.girl = object.idGirl
      dataGirl.id = id
      this.totPrice += object.price
      this.cart.push(dataGirl)  
      id++    
      this.quantity = id
      //console.log("vthis.cart9999",dataGirl)
    });
  }
}
