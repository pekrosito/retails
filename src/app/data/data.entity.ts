  export class product {
    detailProduct:string;
    price:number;
    pic:string;
    available:boolean;
    color:Array<any>;
    sizes:Array<any>;
    //[{name:'XS',selected:true, idSizes:0},{name:'S',selected:false,idSizes:1},{name:'M',selected:false,idSizes:2},{name:'L',selected:false,idSizes:3}], 
    description:string;
    idGirl: number
  }
  export class model {
    name: string;
    idGirl:number;
  }
  export class color {
    value: number;
    name: string;
  }
  export class picForProduct {
    idGirl: number;
    name: string;
    color: number;
  }
  export class carousel1 {
    pic: string;
    active: string;
  }
  export class carousel2 {
    pic: string;
    active: string;
  }
  export class carousel3 {
    pic: string;
    active: string;
  }
  export class shopping{
    girl: number;
    picture: string;
    color: string;
    size: string;
  }
 