import { product, model, color, picForProduct, carousel1, carousel2, carousel3, shopping } from './data.entity';

export const products: product[] =  [
    {detailProduct:'Cazadora Ciclista De Ante Ribete Con Cinturón',
    price:27.56,
    pic:'',
    available:true,
    color:['7','2'],
    sizes:[{name:'M',selected:true,idSizes:2},{name:'L',selected:false,idSizes:3},{name:'XL',selected:false,idSizes:4}], 
    description:'La tela no tiene ninguna elastacidad, confensionada para las temporadas de primavera, otoño. Estilo casual, material utilizado 97% poliéster, 3% lana. Decorado con borlas.',
    idGirl: 1
    },{
    detailProduct:'Blusa Hombro Al Aire Manga Larga',
    price:9.47,
    pic:'',
    available:true,
    color:['1','4','3'],
    sizes:[{name:'XS',selected:true,idSizes:0},{name:'S',selected:false, idSizes:1},{name:'M',selected:false,idSizes:2},{name:'L',selected:false,idSizes:3}], 
    description:'La tela no tiene ninguna elastacidad, confensionada para las temporadas de primavera, otoño. Estilo casual, material utilizado 80% poliéster, 10% lino y 10% algodón.',
    idGirl: 2
    },{
    detailProduct:'Vestido Festoneado Espalda Con Abertura',
    price:6.02,
    pic:'',
    available:true,
    color:['5','6'],
    sizes:[{name:'XS',selected:true, idSizes:0},{name:'S',selected:false,idSizes:1},{name:'M',selected:false,idSizes:2},{name:'L',selected:false,idSizes:3}], 
    description:'La tela no tiene ninguna elastacidad, confensionada para las temporadas de primavera, otoño. Estilo casual, material utilizado 80% poliéster, 10% lino y 10% algodón.',
    idGirl: 3
    }];

export const models: model[] = [
    {name:'verde_001.jpg',idGirl:1},
    {name:'azul_001.jpg',idGirl:2},
    {name:'vino_001.jpg',idGirl:3}];

export const colors: color[] = [ 
    {name:'azul.jpg', value:1},
    {name:'gris.jpg', value:2},
    {name:'rosado.jpg', value:3},
    {name:'mostaza.jpg', value:4},
    {name:'vino.jpg', value:5},
    {name:'fuscia.jpg', value:6},
    {name:'verde.jpg', value:7}];

export const picForProducts: picForProduct[] = [
    {name:'verde_001.jpg',idGirl:1, color:7},
    {name:'verde_002.jpg',idGirl:1, color:7},
    {name:'verde_003.jpg',idGirl:1, color:7},
    {name:'gris_001.jpg',idGirl:1, color:2},
    {name:'gris_002.jpg',idGirl:1, color:2},
    {name:'gris_003.jpg',idGirl:1, color:2},
    {name:'azul_001.jpg',idGirl:2, color:1},
    {name:'azul_002.jpg',idGirl:2, color:1},
    {name:'azul_003.jpg',idGirl:2, color:1},
    {name:'mostaza_001.jpg',idGirl:2, color:4},
    {name:'mostaza_002.jpg',idGirl:2, color:4},
    {name:'mostaza_003.jpg',idGirl:2, color:4},
    {name:'rosa_001.jpg',idGirl:2, color:3},
    {name:'rosa_002.jpg',idGirl:2, color:3},
    {name:'rosa_003.jpg',idGirl:2, color:3},
    {name:'vino_001.jpg',idGirl:3, color:5},
    {name:'vino_002.jpg',idGirl:3, color:5},
    {name:'vino_003.jpg',idGirl:3, color:5},
    {name:'fuscia_001.jpg',idGirl:3, color:6},
    {name:'fuscia_002.jpg',idGirl:3, color:6},
    {name:'fuscia_003.jpg',idGirl:3, color:6}];

export const carouselOne: carousel1[] = [
    {pic:'1.jpg',active:'active'},
    {pic:'2.jpg',active:''},
    {pic:'3.jpg',active:''}];

export const carouselTwo: carousel2[]=[
    {pic:'1.jpg',active:'active'},
    {pic:'2.jpg',active:''},
    {pic:'3.jpg',active:''}];

export const carouselThree: carousel3[]=[
    {pic:'1.jpg',active:'active'},
    {pic:'2.jpg',active:''},
    {pic:'3.jpg',active:''}];
export const shoppings: shopping[]=[]