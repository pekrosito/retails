import { Injectable } from '@angular/core';

import { product, model, color, picForProduct, carousel1, carousel2, carousel3, shopping } from './data.entity';
import { products, models, colors, picForProducts, carouselOne, carouselTwo, carouselThree, shoppings } from './data.mock';

@Injectable()
export class dataService {
    
  getProducts(): Promise<product[]> {
    return Promise.resolve(products);
  }

  getModels(): Promise<model[]> {
    return Promise.resolve(models);
  }

  getColors(): Promise<color[]> {
    return Promise.resolve(colors);
  }

  getPicturesModels(): Promise<picForProduct[]> {
    return Promise.resolve(picForProducts);
  }

  getCarousel1(): Promise<carousel1[]> {
    return Promise.resolve(carouselOne);
  }

  getCarousel2(): Promise<carousel2[]> {
    return Promise.resolve(carouselTwo);
  }

  getCarousel3(): Promise<carousel3[]> {
    return Promise.resolve(carouselThree);
  }

  setShopping(data): Promise<shopping[]>{
    shoppings.push(data)
    return 
  }

  delShopping(data): Promise<shopping[]>{
    var id = shoppings.findIndex(x=>x.girl == data.girl && x.color == data.color)
    shoppings.splice(id,1)
    return 
  }

  getShopping(): Promise<shopping[]>{
    return Promise.resolve(shoppings);
  }
}