import { MatSnackBar } from '@angular/material';
import { MessageService } from '../_services/index';
import { NotificationsService } from 'angular2-notifications';
import { product, model, color, picForProduct, carousel1, carousel2, carousel3 } from '../data/data.entity';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Component, Inject, Injectable, OnDestroy, OnInit } from '@angular/core';
import { dataService } from '../data/data.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  products: product[];
  colorsClothes: color[];
  picForProduct: picForProduct[];
  models: model[];
  carousel1: carousel1[];
  carousel2: carousel2[];
  carousel3: carousel3[];
  colorChick;
  picture: string;
  backgroundStyle = 'white';
  chick = 1;
  btnSelect;
  
  sectionProduct = 'Mis productos';

  constructor(public dialog: MatDialog, private _service: NotificationsService, public snackBar: MatSnackBar, private messageService: MessageService, private dataService: dataService) {}
  
  ngOnInit() {
    this.picture= "none.jpg";
    this.getComponents()
    setTimeout(() =>(this.setValues()), 500);
  }

  getComponents():void {
    this.dataService.getProducts().then(product => this.products = product);
    this.dataService.getColors().then(color => this.colorsClothes = color);
    this.dataService.getPicturesModels().then(pictureModel => this.picForProduct = pictureModel);
    this.dataService.getModels().then(model => this.models = model);
    this.dataService.getCarousel1().then(carousel => this.carousel1 = carousel)
    this.dataService.getCarousel2().then(carousel => this.carousel2 = carousel)
    this.dataService.getCarousel3().then(carousel => this.carousel3 = carousel)
  }

  setValues(){
    var i = this.products.findIndex(x => {return x.idGirl == 1})
    this.colorChick = this.products[i].color[0];
    var girl = this.picForProduct.find(x=>{return x.idGirl === this.products[i].idGirl})
    this.picture = girl.name;
  }

  addCart(message: string, action: string): void {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
    var i = this.products.findIndex(x => {return x.idGirl == this.chick})
    var p = this.products[i].sizes.findIndex(x => {return x.selected == true})
    var c = this.colorsClothes.findIndex(x=>{return x.value == this.colorChick})
    let data = {
      girl: this.chick,
      picture: this.picture,
      color: this.colorsClothes[c].name,
      size: this.products[i].sizes[p].name
    }
    this.messageService.sendMessage(data);
  }
  
  modalSize(): void {
    let dialogRef = this.dialog.open(popUpSizes, {
      width: '350px',
      data: {  }
    });

    dialogRef.afterClosed().subscribe(result => {
      //this.animal = result;
    });
  }

  modalStores(): void {
    let dialogRef = this.dialog.open(popUpMap, {
      width: '450px',
      height: '450px',
      data: {  }
    });

    dialogRef.afterClosed().subscribe(result => {
      //this.animal = result;
    });
  }
                  
  changeImage(value){
    this.chick = value;
    var i = this.products.findIndex(x => {return x.idGirl == this.chick})
    this.colorChick = this.products[i].color[0];
    var i = this.picForProduct.findIndex(x => {return x.color == this.colorChick})
    this.picture = this.picForProduct[i].name
  }       
  
  selectedSize(data){
    var i = this.products.findIndex(x => {return x.idGirl == this.chick})
    var p = this.products[i].sizes.findIndex(x => {return x.idSizes == data.idSizes})
    this.products[i].sizes.forEach(element => {
      if(element.selected)
        element.selected = false;
    });
    this.products[i].sizes[p].selected = this.products[i].sizes[p].selected ? false : true
  }
  
  changeSubImage(pic){      
    this.picture = pic
  }      
  
  changeColor(colorValue){
    this.colorChick = colorValue;
    var i = this.picForProduct.findIndex(x => {return x.color == colorValue})
    this.picture = this.picForProduct[i].name
  }
}

@Component({
  selector: 'app-product',
  templateUrl: '../modals/modalSize.component.html',
})
export class popUpSizes {
  constructor(
    public dialogRef: MatDialogRef<popUpSizes>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
}

@Component({
  selector: 'app-product',
  templateUrl: '../modals/modalMap.component.html',
})
export class popUpMap {
  constructor(
    public dialogRef: MatDialogRef<popUpMap>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
