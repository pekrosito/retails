import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app/home/app.component';


const appRoutes: Routes = [
    { path: '', component: AppComponent },
    { path: 'store', loadChildren: '.app/store/store.module#StoreModule' },
    // otherwise redirect to home
    { path: '**', redirectTo: '', pathMatch: 'full' }
];

export const routing = RouterModule.forRoot(appRoutes);